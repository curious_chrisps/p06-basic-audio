/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.initialiseWithDefaultDevices(2, 2); // 2 inputs, 2 outputs
    audioDeviceManager.addAudioCallback(this);
    audioDeviceManager.setMidiInputEnabled("VMPK Output", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    
    addAndMakeVisible(volumeSlider);
    volumeSlider.setSliderStyle(Slider::LinearVertical);
    volumeSlider.setRange(0, 1);
    volumeSlider.addListener(this);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    volumeSlider.setBounds(20, 20, 25, 150);
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    
}

void MainComponent::audioDeviceIOCallback (const float** inputChannelData,
                            int numInputChannels,
                            float** outputChannelData,
                            int numOutputChannels,
                            int numSamples)
{
//    DBG("The audioDeviceIOCallback (1) function was just called.");
 
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    volume = volumeSlider.getValue();
    
    while (numSamples--)
    {
        *outL = *inL * volume;
        *outR = *inR * volume;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
    
}
void MainComponent::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("The audioDeviceAboutToStart (2) function was just called.");
}

void MainComponent::audioDeviceStopped()
{
    DBG("The audioDeviceStopped (3) function was just called.");
}

void MainComponent::handleIncomingMidiMessage (MidiInput*, const MidiMessage& message)
{
    DBG("Midi Message recieved.");
    
    if (message.isController()) {
        DBG("Controller number: " << message.getControllerNumber());
        DBG("Controller Value: " << message.getControllerValue());
        volumeSlider.getValueObject().setValue(message.getControllerValue()/127.0);
    }
}
